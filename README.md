# Cours Symfony

Démarrer Server Symfony => symfony serve

### Liste des idées

* Moteur de recherche
* Compte User
	** PlayList
	** Compte Author
	** OAuth
* Lecteur Audio - Barre bas de fenêtre
* Like - Favoris
* Home Page
* Pages album - Piste - Artiste
* Page About
* Page Legals

-------------------------------

### Plan du cours du 18/03/2021

Template -  Twig
CSS - JS - Image static (interface) => Assests

### Plan du cours du 19/03/2021

Création page About, Légals et Musique

### Plan du cours du 24/03/2021

Création page publique

------------------------------

### Commande Symfony
* Créer un projet
```
symfony new  --version=lts --full
```

* Pour voir la version de php qui est utilisé
```shell
symfony local:php:list
```

* bundle => bibliothèque dédié à symfony
```shell
symfony composer require symfony/webpack-encore-bundle
```

* Pour installer Bootstrap
```shell
npm install
npm install bootstrap@next
```

* Pour compiler le SCSS en CSS
```shell
npm run watch
```

* Pour la mise en production
```shell
npm run build
```

* Pour arrêter le serveur symfony si il c'est mal quitté
```shell
symfony serve:stop
```

* Pour créer un nouveau controller (page)
```shell
symfony console make:controller
```

* Pour voir les routes
```shell
symfony console debug:route
```

* Pour créer une base de donnée
```shell
symfony console doctrine:database:create
```

* Créer la structure de la base
"Windows"
```shell
symfony console make:entity --no -ansi
```
* Créer la structure de la base
"Autre"
```shell
symfony console make:entity
```

* Pour migrer la base, pour générer les requêtes sql (Créer le fichier migration)
```shell
symfony console make:migration
```

* Pour executer le script dans la base de données (Migrer le fichier migration)
```shell
symfony console migrate
```

* Pour ajouter des associations
```shell
symfony console make:entity Nom_table
```

* Pour créer un espace d'administration (Pour permettre la consultation, création, modification, suppression)
```shell
symfony console make:crud Nom_table
```

* Pour avoir un système pour générer des données pour les BDD en dev
```shell
symfony composer require --dev hautelook/alice-bundle
```

* Pour charger les données du fixture
```shell
symfony console hautelook:fixture:load --purge-with-truncate
```

* Pour vider le cache
```shell
symfony console cache:clear
```

* Pour créer le système d'utilisateur
```shell
symfony console make:user
```

* Pour créer un système d'authenfication
```shell
symfony console make:auth
```

* Formulmaire d'enregistrement
```shell
symfony console make:registration-form
```

* 
```shell

```

