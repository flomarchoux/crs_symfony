<?php


namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class SmartLocaleRedirectSubscriber implements EventSubscriberInterface
{
    private UrlGeneratorInterface $urlGenerator;

    public function __construct(UrlGeneratorInterface $urlGenerator)
    {
        $this->urlGenerator = $urlGenerator;
    }


    public static function getSubscribedEvents()
    {
        return[
          KernelEvents::REQUEST => 'beSmart',
        ];
    }

    public function beSmart(RequestEvent $event)
    {
        /*dump($event);*/
        $request = $event->getRequest();

        if ('/' === $request->getPathInfo()){
            $chosenLangage = 'fr';
            foreach ($request->getLanguages() as $language){
                if (in_array($language, ['fr', 'en'])){
                    $chosenLangage = $language;
                    break;
                }
            }
            $event->setResponse(new RedirectResponse(
                $this->urlGenerator->generate('home', [
                    '_locale' => $chosenLangage,
                ])
            ));
        }

    }

}