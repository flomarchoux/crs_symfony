<?php

namespace App\Controller;

use App\Entity\Album;
use App\Repository\ArtistRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AlbumController extends AbstractController
{
    /**
     * @Route("/album/{id}", name="album")
     */
    public function index(Album $album, ArtistRepository $artistRepository): Response
    {
        $artists = $album->getArtists();

        dump($artists);

        return $this->render('album/index.html.twig', [
            'controller_name' => 'AlbumController',
            'album' => $album,
            'artists' => $artists,
        ]);
    }
}
