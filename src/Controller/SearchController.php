<?php

namespace App\Controller;

use App\Entity\Artist;
use App\Repository\AlbumRepository;
use App\Repository\ArtistRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    /**
     * @Route("/search", name="search")
     */
    public function index(
        Request $request,
        ArtistRepository $artistRepository,
        AlbumRepository $albumRepository
    ): Response {
        if (!$request->query->has('q')){
            /*throw $this->createAccessDeniedException('Not q query given');*/
            return $this->redirectToRoute('home');
        }

        $type = $request->query->get('type', null);
        if (!in_array($type, ['artist','album', null])) {
            throw $this->createAccessDeniedException(sprintf(
                'Wrong param type for search. Given "%s", allowed only "artist" or "album".',
                $type
            ));
        }

        $page = (int) $request->query->get('page', 1);

        if ($page < 1 ){
            $page = 1;
        }

        $limit = null === $type ? 5 : 10;

        $search= $request->query->get('q');
        $respArtist = $artistRepository->findBySearchQuery($search, $limit, $page);
        $respAlbum = $albumRepository->findBySearchQuery($search, $limit, $page);
        $countArtists = $artistRepository->countBySearchQuery($search);
        $countAlbums = $albumRepository->countBySearchQuery($search);

        if ('artist' === $type) {
            $countpage = (int) ceil($countArtists / $limit);
        }
        elseif ('album' === $type) {
            $countpage = (int) ceil($countAlbums /$limit);
        }

        $albums =[];
        $artists =[];

        if (in_array($type, [null, 'artist'])) {
            $artists = $respArtist;
        }

        if (in_array($type, [null, 'album'])) {
            $albums = $respAlbum;
        }

        return $this->render('search/index.html.twig', [
            'controller_name' => 'SearchController',
            'search'=>$search,
            'type'=> $type,
            'artists'=>$artists, //$respArtist,
            'albums'=>$albums, //$respAlbum,
            'page'=> $page,
            'count_artist'=> $countArtists,
            'count_page'=> $countpage ?? 0,
            'count_album'=> $countAlbums,
        ]);
    }
}
