<?php

namespace App\Controller;

use App\Entity\Artist;
use App\Repository\AlbumRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArtistController extends AbstractController
{
    /**
     * @Route("/artist/{id}", name="artist")
     */
    public function index(Artist $artist, AlbumRepository $albumRepository): Response
    {
        $albums = $albumRepository->findByArtist($artist);

        dump($albums);

        return $this->render('artist/index.html.twig', [
            'controller_name' => 'ArtistController',
            'artist' => $artist,
            'albums' => $albums,
        ]);
    }
}
