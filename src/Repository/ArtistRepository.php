<?php

namespace App\Repository;

use App\Entity\Album;
use App\Entity\Artist;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Artist|null find($id, $lockMode = null, $lockVersion = null)
 * @method Artist|null findOneBy(array $criteria, array $orderBy = null)
 * @method Artist[]    findAll()
 * @method Artist[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArtistRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Artist::class);
    }

    public function findBySearchQuery(string $search, int $limit = 5, int $page = 1)
    {
        $qb = $this->getBySearchQuery($search);

        $qb->setMaxResults($limit);
        $qb->setFirstResult(($page - 1) * $limit);

        return $qb->getQuery()->getResult();
    }

    public function countBySearchQuery(string $search): int
    {
        $qb = $this->getBySearchQuery($search);

        $qb->select('COUNT(art.id)');

        return $qb->getQuery()->getSingleScalarResult();
    }

    private function getBySearchQuery(string $search): QueryBuilder
    {
        $qb = $this->createQueryBuilder('art');

        $qb->where('art.name LIKE :searchLike');
        $qb->setParameter('searchLike', '%' . $search . '%');

        return $qb;
    }

    // /**
    //  * @return Artist[] Returns an array of Artist objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Artist
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
