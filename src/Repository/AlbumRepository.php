<?php

namespace App\Repository;

use App\Entity\Album;
use App\Entity\Artist;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Album|null find($id, $lockMode = null, $lockVersion = null)
 * @method Album|null findOneBy(array $criteria, array $orderBy = null)
 * @method Album[]    findAll()
 * @method Album[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AlbumRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Album::class);
    }

    public function findByArtist(Artist $artist): array
    {
        //SELECT alb.* FROM album AS alb
        $qb = $this->createQueryBuilder('alb');

        //INNER JOIN artists AS art ON art.id = alb.artist_id
        $qb->innerJoin('alb.artists', 'art');

        //WHERE art.id = ?
        $qb->where('art.id = :artistid');

        // Add parameter named artistid, use to prevent SQL injection.
        $qb->setParameter('artistid', $artist->getId());

        return $qb->getQuery()->getResult();
    }

    public function findBySearchQuery(string $search, int $limit = 5, int $page = 1)
    {
        $qb = $this->getBySearchQuery($search);

        $qb->setMaxResults($limit);
        $qb->setFirstResult(($page - 1)*$limit );

        return $qb->getQuery()->getResult();
    }

    public function countBySearchQuery(string $search): int
    {
        $qb = $this->getBySearchQuery($search);

        $qb->select('COUNT(alb.id)');

        return $qb->getQuery()->getSingleScalarResult();
    }

    private function getBySearchQuery(string $search): QueryBuilder
    {
        $qb = $this->createQueryBuilder('alb');

        $qb->where('alb.title LIKE :searchLike');
        $qb->setParameter('searchLike','%'.$search.'%');

        return $qb;
    }

    // /**
    //  * @return Album[] Returns an array of Album objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Album
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
