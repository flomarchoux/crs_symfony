<?php


namespace App\Twig\Extention;


use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class DurationExtention extends AbstractExtension
{
    public function getFilters()
    {
        return [
          new TwigFilter('duration',[$this, 'duration'])
        ];
    }

    public function duration(int $duration):string
    {
        $seconds = $duration % 60;
        $totalMinutes = floor($duration / 60);
        $hours = (int) floor($totalMinutes / 60);
        $minutes = $totalMinutes % 60;

        $txt = sprintf('%2d:%2d', $minutes, $seconds);

        if ($hours > 0){
            $txt = $hours . ':' . $txt;
        }

        return $txt;
    }
}