<?php

namespace App\Form\Admin;

use App\Entity\Artist;
use App\Entity\Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ArtistType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description')
            ->add('photo')
            ->add('wikiId')
            ->add('types', EntityType::class, [
                'class' => Type::class,
                'multiple' => true,
                'choice_label' => function(Type $type){
                    return sprintf('%d - %s', $type->getId(), $type->getLabel());
                },
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Artist::class,
        ]);
    }
}
