<?php

namespace App\Form\Admin;

use App\Entity\Album;
use App\Entity\Artist;
use App\Entity\Type;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Bridge\Doctrine\Form\DataTransformer\CollectionToArrayTransformer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AlbumType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('releasedAt')
            ->add('image')
            ->add('types', EntityType::class, [
                'class' => Type::class,
                'multiple' => true,
                'choice_label' => function(Type $type){
                    return sprintf('%d - %s', $type->getId(), $type->getLabel());
                },
            ])
            ->add('artists', EntityType::class, [
                'class' => Artist::class,
                'multiple' => true,
                'choice_label' => 'Name',
            ])
            ->add('tracks', CollectionType::class,[
                'entry_type' => AdminAlbumTrackType::class,
                'allow_add' => true,
                'allow_delete' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Album::class,
        ]);
    }
}
