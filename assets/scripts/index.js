import {Player} from './player';
import {PlayerUi} from './player-ui';

const player = new Player();
new PlayerUi(player);

document.querySelectorAll('[data-play]').forEach(($play) => {
    const url = $play.getAttribute('data-play');

    $play.addEventListener('click', () => {
        player.setMusic(url);
    });
});