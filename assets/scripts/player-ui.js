import {Player} from './player';

/**
 * Class to make interact between User interface (HTML) and a Player instance.
 */
export class PlayerUi {
    /**
     * @type {Player}
     * @private
     */
    _player;

    /**
     * @type {HTMLElement}
     * @private
     */
    _$playerBar;

    /**
     * @type {HTMLElement}
     * @private
     */
    _$playBtn;

    /**
     * @type {HTMLElement}
     * @private
     */
    _$pauseBtn;

    /**
     * @type {HTMLElement}
     * @private
     */
    _$backwardBtn;

    /**
     * @type {HTMLElement}
     * @private
     */
    _$forwardBtn;

    /**
     * @type {HTMLElement}
     * @private
     */
    _$totalDuration;

    /**
     * @type {HTMLElement}
     * @private
     */
    _$currentTime;

    /**
     * @type {HTMLInputElement}
     * @private
     */
    _$cursor;

    constructor(player, $playerBar = document.querySelector('#bottom-player')) {
        if (!(player instanceof Player)) {
            throw new Error('The first argument has to be an instance of Player class.');
        }

        if (!($playerBar instanceof HTMLElement)) {
            throw new Error('The second argument has to be an instance of HTMLElement class.');
        }

        this._player = player;
        this._$playerBar = $playerBar;

        this._initInterfaceButtons();
        this._initInterfaceEventListeners();
        this._initPlayerListeners();
    }

    /**
     * Retrieve interface elements references.
     * @private
     */
    _initInterfaceButtons() {
        this._$playBtn = this._$playerBar.querySelector('[data-player-play]');
        this._$pauseBtn = this._$playerBar.querySelector('[data-player-pause]');
        this._$backwardBtn = this._$playerBar.querySelector('[data-player-backward]');
        this._$forwardBtn = this._$playerBar.querySelector('[data-player-forward]');

        this._$totalDuration = this._$playerBar.querySelector('[data-player-total-duration]');
        this._$currentTime = this._$playerBar.querySelector('[data-player-current-time]');
        this._$cursor = this._$playerBar.querySelector('[data-player-cursor]');
    }

    /**
     * Register of event listeners on interface elements.
     * @private
     */
    _initInterfaceEventListeners() {
        if (this._$playBtn) {
            this._$playBtn.addEventListener('click', () => {
                this._player.play();
                this._$playBtn.hidden = true;
                this._$pauseBtn.hidden = false;
            });
        }

        if (this._$pauseBtn) {
            this._$pauseBtn.addEventListener('click', () => {
                this._player.pause();
                this._$pauseBtn.hidden = true;
                this._$playBtn.hidden = false;
            });
        }

        if (this._$forwardBtn) {
            this._$forwardBtn.addEventListener('click', () => {
                this._player.forwardOf();
            });
        }

        if (this._$backwardBtn) {
            this._$backwardBtn.addEventListener('click', () => {
                this._player.backwardOf();
            });
        }
    }

    /**
     * Register listener send by the Player instance. Eg: timer refresh every seconds while playing music.
     * @private
     */
    _initPlayerListeners() {
        this._player.addListener('play', () => {
            if (this._$playBtn) {
                this._$playBtn.hidden = true;
            }

            if (this._$pauseBtn) {
                this._$pauseBtn.hidden = false;
            }

            if (this._$totalDuration) {
                this._$totalDuration.innerHTML = this._displayTime(this._player.getCurrentPlayedMusicTotalDuration());
            }

            if (this._$cursor) {
                this._$cursor.max = this._displayTime(this._player.getCurrentPlayedMusicTotalDuration());
            }
        });

        this._player.addListener('pause', () => {
            if (this._$pauseBtn) {
                this._$pauseBtn.hidden = true;
            }

            if (this._$playBtn) {
                this._$playBtn.hidden = false;
            }
        });

        this._player.addListener('timer', (seek) => {
            if (this._$currentTime) {
                this._$currentTime.innerHTML = this._displayTime(seek);
            }

            if (this._$cursor) {
                this._$cursor.value = seek;
            }
        });
    }

    /**
     * Format seconds time like [hours:]min:seconds
     * 01:02 | 10:30 | 1:52:12
     *
     * @param time {number}
     * @return {string}
     * @private
     */
    _displayTime(time) {
        time = Math.round(time);

        let seconds = (time % 60).toString();
        const totalMinutes = Math.floor(time / 60);
        const hours = Math.floor(totalMinutes / 60);
        let minutes = (totalMinutes % 60).toString();

        seconds = seconds.length === 1 ? '0' + seconds : seconds;
        minutes = minutes.length === 1 ? '0' + minutes : minutes;

        let txt = minutes + ':' + seconds;
        if (hours > 0) {
            txt = hours + ':' + txt;
        }

        return txt;
    }
}