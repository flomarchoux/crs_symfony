import {Howl} from 'howler';

/**
 * Class to control the audio API via Howler.js.
 */
export class Player {
    /**
     * @type {Howl|null}
     * @private
     */
    _howler = null;

    /**
     * @type {number}
     * @private
     */
    _currentPlayIndex = 0;

    /**
     * @type {[]}
     * @private
     */
    _musics = [];

    /**
     * @type {Map<string, Function[]>}
     * @private
     */
    _listeners = new Map();

    constructor() {
        let timeoutId = null;

        this.addListener('play', () => {
            if (null !== timeoutId) {
                clearTimeout(timeoutId);
            }

            const func = () => {
                timeoutId = setTimeout(() => {
                    this._emit('timer', this.getCurrentPlayedMusicSeek());
                    func();
                }, 100);
            };

            func();
        });

        this.addListener('stop', () => {
            if (null !== timeoutId) {
                clearTimeout(timeoutId);
            }

            this._emit('timer', 0);
        });

        this.addListener('pause', () => {
            if (null !== timeoutId) {
                clearTimeout(timeoutId);
            }
        });
    }

    pause() {
        if (null !== this._howler) {
            this._howler.pause();
        }
    }

    play() {
        if (null !== this._howler) {
            this._howler.play();
        }
    }

    forwardOf(duration = 5) {
        this.goTo(this.getCurrentPlayedMusicSeek() + duration);
    }

    backwardOf(duration = 5) {
        this.forwardOf(-duration);
    }

    stop() {
        if (null !== this._howler) {
            this._howler.stop();
        }
    }

    nextTrack() {
        this._currentPlayIndex++;

        if (this._musics.length < this._currentPlayIndex) {
            this._currentPlayIndex = 0;
        }

        if (null !== this._howler) {
            this._howler.seek(0, this._currentPlayIndex);
        }
    }

    prevTrack() {
        this._currentPlayIndex--;

        if (0 > this._currentPlayIndex) {
            this._currentPlayIndex = this._musics.length - 1;
        }

        if (null !== this._howler) {
            this._howler.seek(0, this._currentPlayIndex);
        }
    }

    goTo(pos) {
        if (0 > pos) {
            pos = 0;
        }

        if (this.getCurrentPlayedMusicTotalDuration() < pos) {
            pos = 0;
        }

        if (null !== this._howler) {
            this._howler.seek(pos);
        }
    }

    getCurrentPlayedMusic() {

    }

    getCurrentPlayedMusicTotalDuration() {
        if (null === this._howler) {
            return 0;
        }

        return this._howler.duration();
    }

    getCurrentPlayedMusicSeek() {
        if (null === this._howler) {
            return 0;
        }

        return this._howler.seek();
    }

    isPlaying() {
        if (null === this._howler) {
            return false;
        }

        return this._howler.playing();
    }

    hasMusicsInPlaylist() {
        return this._musics.length > 0;
    }

    addMusic(music) {
        this._musics.push(music);

        // Each time a music is add, we have to reset the howler instance.
        this._initOrResetHowler();
    }

    setMusic(music, autoPlay = true) {
        this._musics = [];
        this.stop();
        this.addMusic(music);

        if (autoPlay) {
            this.play();
        }
    }

    /**
     * @param event {string}
     * @param callback {Function}
     */
    addListener(event, callback) {
        if (!this._listeners.has(event)) {
            this._listeners.set(event, []);
        }

        this._listeners.get(event).push(callback);
    }

    _initOrResetHowler() {
        const currentSeek = this.getCurrentPlayedMusicSeek();
        const playing = this.isPlaying();

        if (null !== this._howler) {
            // Destroy all cache and sound data from the instance.
            // Be sure to clean audio before start a new one.
            this._howler.unload();
        }

        this._howler = new Howl({
            src: this._musics,
        });

        for (const event of ['play', 'pause', 'stop']) {
            if (!this._listeners.has(event)) {
                continue;
            }

            this._howler.on(event, () => {
                this._emit(event);
            });
        }

        if (this._musics.length === 1) {
            this._howler.seek(currentSeek);
        } else {
            this._howler.seek(currentSeek, this._currentPlayIndex);
        }

        if (playing) {
            this.play();
        }
    }

    /**
     * @param event {string}
     * @param data {object|undefined}
     * @private
     */
    _emit(event, data = undefined) {
        if (this._listeners.has(event)) {
            for (const listener of this._listeners.get(event)) {
                listener(data);
            }
        }
    }
}